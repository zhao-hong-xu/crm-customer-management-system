package com.javastudy.crm.workbench.service.impl;

import com.javastudy.crm.commons.contants.Contants;
import com.javastudy.crm.commons.utils.DateUtils;
import com.javastudy.crm.commons.utils.UUIDUtils;
import com.javastudy.crm.settings.domain.User;
import com.javastudy.crm.workbench.domain.*;
import com.javastudy.crm.workbench.mapper.*;
import com.javastudy.crm.workbench.service.ClueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class ClueServiceImpl implements ClueService {
    @Autowired
    private ClueMapper clueMapper;
    @Autowired
    private CustomerMapper customerMapper;
    @Autowired
    private ContactsMapper contactsMapper;
    @Autowired
    private ClueRemarkMapper clueRemarkMapper;
    @Autowired
    private CustomerRemarkMapper customerRemarkMapper;
    @Autowired
    private ContactsRemarkMapper contactsRemarkMapper;
    @Autowired
    private ClueActivityRelationMapper clueActivityRelationMapper;
    @Autowired
    private ContactsActivityRelationMapper contactsActivityRelationMapper;
    @Autowired
    private TranMapper tranMapper;
    @Autowired
    private TranRemarkMapper tranRemarkMapper;
    @Override
    public int saveCreateClue(Clue clue) {
        return clueMapper.insertClue(clue);
    }

    @Override
    public List<Clue> queryClueByConditionForPage(Map<String, Object> map) {
        return clueMapper.selectClueByConditionForPage(map);
    }

    @Override
    public int queryClueCountByCondition(Map<String, Object> map) {
        return clueMapper.selectClueCountByCondition(map);
    }

    @Override
    public Clue queryClueById(String id) {
        return clueMapper.selectClueById(id);
    }

    @Override
    public int deleteClueByIds(String[] ids) {
        return clueMapper.deleteClueByIds(ids);
    }

    @Override
    public int saveEditClue(Clue clue) {
        return clueMapper.updateClueByClue(clue);
    }

    @Override
    public Clue queryClueForDetailById(String id) {
        return clueMapper.selectClueForDetailById(id);
    }

    @Override
    public void saveConvert(Map<String, Object> map) {
        User user =(User) map.get(Contants.SESSION_USER);
        String clueId = (String) map.get("clueId");
        String isCreateTran = (String) map.get("isCreateTran");
        //根据id查询线索信息
        Clue clue = clueMapper.selectClueById(clueId);
        //向客户对象封装数据
        Customer customer=new Customer();
        customer.setId(UUIDUtils.getUUID());
        customer.setOwner(user.getId());
        customer.setName(clue.getCompany());
        customer.setWebsite(clue.getWebsite());
        customer.setPhone(clue.getPhone());
        customer.setCreateBy(user.getId());
        customer.setCreateTime(DateUtils.formateDate(new Date()));
        customer.setContactSummary(clue.getContactSummary());
        customer.setNextContactTime(clue.getNextContactTime());
        customer.setDescription(clue.getDescription());
        customer.setAddress(clue.getAddress());
        //调用mapper层方法，把该线索中有关公司的信息转换到创建的客户中
        int i = customerMapper.insertCreateCustomer(customer);
        //向联系人封装数据
        Contacts contacts=new Contacts();
        contacts.setId(UUIDUtils.getUUID());
        contacts.setOwner(user.getId());
        contacts.setSource(clue.getSource());
        contacts.setCustomerId(customer.getId());
        contacts.setFullname(clue.getFullname());
        contacts.setAppellation(clue.getAppellation());
        contacts.setEmail(clue.getEmail());
        contacts.setMphone(clue.getMphone());
        contacts.setJob(clue.getJob());
        contacts.setCreateBy(user.getId());
        contacts.setCreateTime(DateUtils.formateDate(new Date()));
        contacts.setDescription(clue.getDescription());
        contacts.setContactSummary(clue.getContactSummary());
        contacts.setNextContactTime(clue.getNextContactTime());
        contacts.setAddress(clue.getAddress());
        //调用mapper层方法，把该线索中有关公司的信息转换到创建的联系人表中
        contactsMapper.insertCreateContacts(contacts);
        //根据clueID查询该线索下所有的备注
        List<ClueRemark> remarkList = clueRemarkMapper.selectClueRemarkForConvertByClueId(clueId);


        if (remarkList!=null &&remarkList.size()>0){
            //遍历线索备注集合，并把数据封装到客户备注和联系人备注中
            CustomerRemark customerRemark=null;
            ContactsRemark contactsRemark=null;
            List<CustomerRemark> customerRemarkList=new ArrayList<>();
            List<ContactsRemark> contactsRemarkList=new ArrayList<>();

            for(ClueRemark cr:remarkList){
                //封装客户备注
                customerRemark=new CustomerRemark();
                customerRemark.setId(UUIDUtils.getUUID());
                customerRemark.setNoteContent(cr.getNoteContent());
                customerRemark.setCreateBy(cr.getCreateBy());
                customerRemark.setCreateTime(cr.getCreateTime());
                customerRemark.setEditBy(cr.getEditBy());
                customerRemark.setEditTime(cr.getEditTime());
                customerRemark.setEditFlag(cr.getEditFlag());
                customerRemark.setCustomerId(customer.getId());
                //封装联系人备注
                contactsRemark=new ContactsRemark();
                contactsRemark.setId(UUIDUtils.getUUID());
                contactsRemark.setNoteContent(cr.getNoteContent());
                contactsRemark.setCreateBy(cr.getCreateBy());
                contactsRemark.setCreateTime(cr.getCreateTime());
                contactsRemark.setEditBy(cr.getEditBy());
                contactsRemark.setEditTime(cr.getEditTime());
                contactsRemark.setEditFlag(cr.getEditFlag());
                contactsRemark.setContactsId(contacts.getId());
                //封装数据到各自的集合中
                customerRemarkList.add(customerRemark);
                contactsRemarkList.add(contactsRemark);
            }
            //把该线索下所有备注转换到客户备注表中一份
            customerRemarkMapper.insertCreateCustomerRemarkByList(customerRemarkList);
            //把该线索下所有备注转换到联系人备注表中一份
            contactsRemarkMapper.insertCreateContactsRemark(contactsRemarkList);
        }


        //根据clueID查询该线索和市场活动的关联关系
        List<ClueActivityRelation> carList = clueActivityRelationMapper.selectClueActivityRelationByClueId(clueId);
        if (carList!=null &&carList.size()>0){
            //把该线索和市场活动的关联关系转换到联系人和市场活动的关联关系表中
            ContactsActivityRelation contactsActivityRelation=null;
            List<ContactsActivityRelation> contactsActivityRelationList=new ArrayList<>();
            for (ClueActivityRelation clueActivityRelation:carList){
                contactsActivityRelation=new ContactsActivityRelation();
                contactsActivityRelation.setId(UUIDUtils.getUUID());
                contactsActivityRelation.setActivityId(clueActivityRelation.getActivityId());
                contactsActivityRelation.setContactsId(contacts.getId());
                contactsActivityRelationList.add(contactsActivityRelation);
            }
            contactsActivityRelationMapper.insertCreateContactsActivityRelation(contactsActivityRelationList);
        }

        //如果需要创建交易，则往交易表中添加一条记录
        if ("true".equals(isCreateTran)){
            Tran tran=new Tran();
            tran.setId(UUIDUtils.getUUID());
            tran.setOwner(user.getId());
            tran.setMoney((String) map.get("money"));
            tran.setName((String) map.get("name"));
            tran.setExpectedDate((String) map.get("expectedDate"));
            tran.setCustomerId(customer.getId());
            tran.setStage((String) map.get("stage"));
            tran.setActivityId((String) map.get("activityId"));
            tran.setContactsId(contacts.getId());
            tran.setCreateBy(user.getId());
            tran.setCreateTime(DateUtils.formateDate(new Date()));
            int i2 = tranMapper.insertCreateTran(tran);
            //如果需要创建交易，则还需要把该线索下所有备注转换到交易备注表中一份
            if (remarkList!=null &&remarkList.size()>0) {
                List<TranRemark> tranRemarkList = new ArrayList<>();
                TranRemark tranRemark = null;
                for (ClueRemark cr : remarkList) {
                    tranRemark = new TranRemark();
                    tranRemark.setId(UUIDUtils.getUUID());
                    tranRemark.setNoteContent(cr.getNoteContent());
                    tranRemark.setCreateBy(cr.getCreateBy());
                    tranRemark.setCreateTime(cr.getCreateTime());
                    tranRemark.setEditBy(cr.getEditBy());
                    tranRemark.setEditTime(cr.getEditTime());
                    tranRemark.setEditFlag(cr.getEditFlag());
                    tranRemark.setTranId(tran.getId());
                    tranRemarkList.add(tranRemark);
                }
                tranRemarkMapper.insertCreateTranRemarkByList(tranRemarkList);
            }

        }

        //删除该线索下所有的备注
        clueRemarkMapper.deleteAllRemarkByClueId(clueId);
        //删除该线索和市场活动的关联关系
        clueActivityRelationMapper.deleteClueActivityRelationByClueId(clueId);

        //删除该线索
        clueMapper.deleteClueById(clueId);
    }
}
