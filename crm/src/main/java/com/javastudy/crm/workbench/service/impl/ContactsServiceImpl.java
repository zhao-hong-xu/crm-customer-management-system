package com.javastudy.crm.workbench.service.impl;

import com.javastudy.crm.workbench.domain.Contacts;
import com.javastudy.crm.workbench.mapper.ContactsMapper;
import com.javastudy.crm.workbench.service.ContactsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ContactsServiceImpl implements ContactsService {
    @Autowired
    private ContactsMapper contactsMapper;
    @Override
    public List<Contacts> queryAllContacts() {
        return contactsMapper.selectAllContacts();
    }
}
