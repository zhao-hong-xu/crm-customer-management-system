package com.javastudy.crm.workbench.service.impl;

import com.javastudy.crm.workbench.domain.ClueActivityRelation;
import com.javastudy.crm.workbench.mapper.ClueActivityRelationMapper;
import com.javastudy.crm.workbench.service.ClueActivityRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClueActivityRelationServiceImpl implements ClueActivityRelationService {
    @Autowired
    private ClueActivityRelationMapper clueActivityRelationMapper;

    @Override
    public int saveRelateClueAndActivity(List<ClueActivityRelation> list) {
        return clueActivityRelationMapper.insertRelateClueAndActivity(list);
    }

    @Override
    public int deleteClueActivityRelationByClueIdAndActivityId(ClueActivityRelation clueActivityRelation) {
        return clueActivityRelationMapper.deleteClueActivityRelationByClueIdAndActivityId(clueActivityRelation);
    }
}
