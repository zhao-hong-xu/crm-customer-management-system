package com.javastudy.crm.workbench.web.controller;

import com.javastudy.crm.settings.domain.User;
import com.javastudy.crm.settings.service.UserService;
import com.javastudy.crm.workbench.domain.Customer;
import com.javastudy.crm.workbench.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class CustomerController {
    @Autowired
    private CustomerService customerService;
    @Autowired
    private UserService userService;

    @RequestMapping("/workbench/customer/toIndex.do")
    public String toCustomerIndex(HttpServletRequest request){
        List<Customer> customers = customerService.queryAllCustomer();
        List<User> users = userService.queryAllUsers();
        request.setAttribute("CustomerList",customers);
        request.setAttribute("userList",users);
        return "/workbench/customer/index";
    }

    @RequestMapping("/workbench/contacts/queryCustomers.do")
    @ResponseBody
    public List<Customer> queryCunstomers(){
        List<Customer> customers = customerService.queryAllCustomer();
        return customers;
    }
}
