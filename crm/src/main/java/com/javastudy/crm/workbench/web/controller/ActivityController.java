package com.javastudy.crm.workbench.web.controller;

import com.javastudy.crm.commons.contants.Contants;
import com.javastudy.crm.commons.domain.ReturnObject;
import com.javastudy.crm.commons.utils.DateUtils;
import com.javastudy.crm.commons.utils.HSSFUtils;
import com.javastudy.crm.commons.utils.UUIDUtils;
import com.javastudy.crm.settings.domain.User;
import com.javastudy.crm.settings.service.UserService;
import com.javastudy.crm.workbench.domain.Activity;
import com.javastudy.crm.workbench.domain.ActivityRemark;
import com.javastudy.crm.workbench.service.ActivityRemarkService;
import com.javastudy.crm.workbench.service.ActivityService;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;


import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;

import java.util.*;

@Controller
public class ActivityController {
    @Autowired
    UserService userService;
    @Autowired
    ActivityService activityService;
    @Autowired
    ActivityRemarkService activityRemarkService;

    @RequestMapping("/workbench/activity/index.do")
    public String toIndex(HttpServletRequest request){
    List<User> users = userService.queryAllUsers();
    request.setAttribute("userList",users);
    return "workbench/activity/index";
    }

    @RequestMapping("/workbench/activity/saveCreateActivity.do")
    @ResponseBody
    public  Object saveCreateActivity(Activity activity,HttpServletRequest request){
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(Contants.SESSION_USER);
        activity.setCreateBy(user.getId());
        activity.setId(UUIDUtils.getUUID());
        activity.setCreateTime(DateUtils.formateDate(new Date()));
        ReturnObject returnObject=new ReturnObject();
        try {
            //调用服务层
            int i = activityService.saveCreateActivity(activity);
            if (i==1){
              returnObject.setCode(Contants.RETURN_OBJECT_CODE_SUCCESS);
            }else{
                returnObject.setCode(Contants.RETURN_OBJECT_CODE_FAIL);
                returnObject.setMassage("系统正忙，请稍后再试...");
            }
        }catch (Exception e){
            e.printStackTrace();
            returnObject.setCode(Contants.RETURN_OBJECT_CODE_FAIL);
            returnObject.setMassage("系统正忙，请稍后再试...");
        }
        return returnObject;
    }

    @RequestMapping("/workbench/activity/queryActivityByConditionForPage.do")
    @ResponseBody
    public Object queryActivityByConditionForPage(String name,String owner,String startDate,String endDate,int pageNo
            ,int pageSize){
        Map<String,Object> map = new HashMap<>();
        map.put("name",name);
        map.put("owner",owner);
        map.put("startDate",startDate);
        map.put("endDate",endDate);
        map.put("beginNo",(pageNo-1)*pageSize);
        map.put("pageSize",pageSize);
        //调用service层方法，查询数据
        List<Activity> activityList=activityService.queryActivityByConditionForPage(map);
        Integer totalRows=activityService.queryCountOfActivityByCondition(map);
        //根据查询结果，成成响应数据
        Map<String,Object> retMap=new HashMap<>();
        retMap.put("activityList",activityList);
        retMap.put("totalRows",totalRows);
        return retMap;

    }
    @RequestMapping("/workbench/activity/deleteActivityByIds.do")
    @ResponseBody
    public Object deleterActivityByIds(String[] id){
        ReturnObject rto=new ReturnObject();
       try{
           int ret=activityService.deleterActivityByIds(id);
           if (ret>0){
               rto.setCode(Contants.RETURN_OBJECT_CODE_SUCCESS);
           }else {
               rto.setCode(Contants.RETURN_OBJECT_CODE_FAIL);
               rto.setMassage("系统忙，请稍后重试！");
           }
       }catch (Exception e){
           e.printStackTrace();
           rto.setCode(Contants.RETURN_OBJECT_CODE_FAIL);
           rto.setMassage("系统忙，请稍后重试！");
       }
        return rto;
    }

    @RequestMapping("/workbench/activity/queryActivityById.do")
    @ResponseBody
    public Object queryActivityById(String id){
        Activity activity = activityService.queryActivityById(id);
        return activity;
    }
    @RequestMapping("/workbench/activity/saveEditActivity.do")
    @ResponseBody
    public  Object saveEditActivity(Activity activity,HttpServletRequest request){
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(Contants.SESSION_USER);
        activity.setEditBy(user.getId());
        activity.setEditTime(DateUtils.formateDate(new Date()));
        ReturnObject returnObject=new ReturnObject();
        try {
            int i = activityService.saveEditActivity(activity);
            if (i>0){
                returnObject.setCode(Contants.RETURN_OBJECT_CODE_SUCCESS);
            }else{
              returnObject.setCode(Contants.RETURN_OBJECT_CODE_FAIL);
              returnObject.setMassage("系统忙，请稍后再试！");
            }
        }catch (Exception e){
            e.printStackTrace();
            returnObject.setCode(Contants.RETURN_OBJECT_CODE_FAIL);
            returnObject.setMassage("系统忙，请稍后再试！");
        }
        return returnObject;
    }

    /**
     * 实验：将数据以Excel格式导出到浏览器
     * @param response
     * @throws IOException
     */
    @RequestMapping("/workbench/activity/fileDownLoad.do")
    public void fileDownLoad(HttpServletResponse response) throws IOException {
        //设置响应类型
        response.setContentType("application/octet-stream;charset=UTF-8");
        //获取输出流
        OutputStream outputStream = response.getOutputStream();
        //浏览器接收到响应信息之后，默认情况下，直接在显示窗口中打开响应信息，及时打不开，也会调用应用程序打开、
        //设置响应头信息，是浏览器接收到相应信息之后，直接激活文件下载窗口，即使能打开也不打开
        response.addHeader("Content-Disposition","attachment;filename=mystudendList.xls");

        //读取Excel文件（InputStream），把文件输出到浏览器（OutputStream）
        FileInputStream fis = new FileInputStream("D:\\zjjrj\\IdeaProjects\\crm-project\\crm\\");

        byte[] b=new byte[256];
        int len=0;
        while ((len=fis.read(b))!=-1){
            outputStream.write(b,0,len);
        }
        //关闭资源
        fis.close();
        outputStream.flush();
    }
    @RequestMapping("/workbench/activity/exportAllActivities.do")
    public void exportAllActivities(HttpServletResponse response) throws IOException {
        List<Activity> activityList=activityService.queryAllActivities();
        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet = wb.createSheet("市场活动列表");
        HSSFRow row = sheet.createRow(0);
        row.createCell(0).setCellValue("ID");
        row.createCell(1).setCellValue("所有者");
        row.createCell(2).setCellValue("活动名称");
        row.createCell(3).setCellValue("开始日期");
        row.createCell(4).setCellValue("结束日期");
        row.createCell(5).setCellValue("开销");
        row.createCell(6).setCellValue("描述");
        row.createCell(7).setCellValue("创建时间");
        row.createCell(8).setCellValue("创建者");
        row.createCell(9).setCellValue("修改时间");
        row.createCell(10).setCellValue("修改者");
        //遍历集合，将集合中的数据填到创建的表格中
        if (activityList!=null &&activityList.size()>0){
            int i=0;
            for (Activity activity:activityList){
                row=sheet.createRow(++i);
                row.createCell(0).setCellValue(activity.getId());
                row.createCell(1).setCellValue(activity.getOwner());
                row.createCell(2).setCellValue(activity.getName());
                row.createCell(3).setCellValue(activity.getStartDate());
                row.createCell(4).setCellValue(activity.getEndDate());
                row.createCell(5).setCellValue(activity.getCost());
                row.createCell(6).setCellValue(activity.getDescription());
                row.createCell(7).setCellValue(activity.getCreateTime());
                row.createCell(8).setCellValue(activity.getCreateBy());
                row.createCell(9).setCellValue(activity.getEditTime());
                row.createCell(10).setCellValue(activity.getEditBy());
            }
        }
       /* //根据wb对象生成Excel文件
        FileOutputStream fos = new FileOutputStream("D:\\zjjrj\\IdeaProjects\\crm-project\\crm\\activityList.xls");
        wb.write(fos);
        //关闭资源
        fos.close();
        wb.close();*/

        //把生成的Excel文件下载到客户端
        response.setContentType("application/ectet-stream;charset=UTF-8");
        response.addHeader("Content-Disposition","attachment;filename=activityList.xls");
        OutputStream os = response.getOutputStream();
       /* InputStream fis = new FileInputStream("D:\\zjjrj\\IdeaProjects\\crm-project\\crm\\activityList.xls");
        byte[] bytes = new byte[256];
        int len=0;
        while ((len=fis.read(bytes))!=-1){
            os.write(bytes,0,len);
        }
        fis.close();*/
        //直接把数据从workbook内存写到浏览器内存
        wb.write(os);
        //关闭资源，刷硬盘
        wb.close();
        os.flush();
    }
    @RequestMapping("/workbench/activity/exportXzActivitiesByIds.do")
    public void exportXzActivities(String[] id,HttpServletResponse response) throws IOException {
        List<Activity> activityList=activityService.queryXzActivitiesByIds(id);
        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet = wb.createSheet("市场活动列表");
        HSSFRow row = sheet.createRow(0);
        row.createCell(0).setCellValue("ID");
        row.createCell(1).setCellValue("所有者");
        row.createCell(2).setCellValue("活动名称");
        row.createCell(3).setCellValue("开始日期");
        row.createCell(4).setCellValue("结束日期");
        row.createCell(5).setCellValue("开销");
        row.createCell(6).setCellValue("描述");
        row.createCell(7).setCellValue("创建时间");
        row.createCell(8).setCellValue("创建者");
        row.createCell(9).setCellValue("修改时间");
        row.createCell(10).setCellValue("修改者");
        //遍历集合，将集合中的数据填到创建的表格中
        if (activityList!=null &&activityList.size()>0){
            int i=0;
            for (Activity activity:activityList){
                row=sheet.createRow(++i);
                row.createCell(0).setCellValue(activity.getId());
                row.createCell(1).setCellValue(activity.getOwner());
                row.createCell(2).setCellValue(activity.getName());
                row.createCell(3).setCellValue(activity.getStartDate());
                row.createCell(4).setCellValue(activity.getEndDate());
                row.createCell(5).setCellValue(activity.getCost());
                row.createCell(6).setCellValue(activity.getDescription());
                row.createCell(7).setCellValue(activity.getCreateTime());
                row.createCell(8).setCellValue(activity.getCreateBy());
                row.createCell(9).setCellValue(activity.getEditTime());
                row.createCell(10).setCellValue(activity.getEditBy());
            }
        }
        //把生成的Excel文件下载到客户端
        response.setContentType("application/ectet-stream;charset=UTF-8");
        response.addHeader("Content-Disposition","attachment;filename=checkedActivityList.xls");
        OutputStream os = response.getOutputStream();
        //直接把数据从workbook内存写到浏览器内存
        wb.write(os);
        //关闭资源，刷硬盘
        wb.close();
        os.flush();

    }

    @RequestMapping("/workbench/activity/importActivities.do")
    @ResponseBody
    public Object importActivities(MultipartFile activityFile,HttpServletRequest request){
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(Contants.SESSION_USER);
        ReturnObject returnObject=new ReturnObject();
        try{
           /* //把excel文件写到磁盘目录中
            String originalFilename = activityFile.getOriginalFilename();
            File file = new File("D:\\zjjrj\\IdeaProjects\\crm-project\\crm\\xlsFiles" + originalFilename);
            activityFile.transferTo(file);

            //解析Excel文件，获取文件中的数据，并且封装成activityList
            InputStream is=
                    new FileInputStream("D:\\zjjrj\\IdeaProjects\\crm-project\\crm\\xlsFiles" + originalFilename);*/
            InputStream is = activityFile.getInputStream();
            HSSFWorkbook workbook = new HSSFWorkbook(is);
            HSSFSheet sheet = workbook.getSheetAt(0);//根据下标获取第一页数据
            HSSFRow row=null;
            HSSFCell cell=null;
            Activity activity=null;
            List<Activity> activityList=new ArrayList<>();

            for (int i=1;i<=sheet.getLastRowNum();i++){  //sheet.getLastRowNum()方法是获取最后一行的下标
                 row = sheet.getRow(i);//获取行的下标，从0开始，依次增加
                activity=new Activity();
                activity.setId(UUIDUtils.getUUID());
                activity.setOwner(user.getId());
                activity.setCreateTime(DateUtils.formateDate(new Date()));
                activity.setCreateBy(user.getId());
                 for (int j=0;j<row.getLastCellNum();j++){  //row.getLastCellNum()方法是获取最后一个单元格的下标+1
                     cell=row.getCell(j); //获取单元格的下标，从0开始，依次增加
                     //获取列中的数据
                     String cellValue= HSSFUtils.getCellValueForStr(cell);
                     if (j==0){
                         activity.setName(cellValue);
                     }else if (j==1){
                         activity.setStartDate(cellValue);
                     }else if (j==2){
                         activity.setEndDate(cellValue);
                     }else if (j==3){
                         activity.setCost(cellValue);
                     }else if (j==4){
                         activity.setDescription(cellValue);
                     }
                 }
                //把每一行中所有列都封装完成之后，把activity保存到list中
                activityList.add(activity);
            }
            //调用service层方法，保存市场活动
            int ret = activityService.saveActivitiesByList(activityList);
            returnObject.setCode(Contants.RETURN_OBJECT_CODE_SUCCESS);
            returnObject.setRetData(ret);
        }catch (Exception e){
            e.printStackTrace();
            returnObject.setCode(Contants.RETURN_OBJECT_CODE_FAIL);
            returnObject.setMassage("系统忙，请稍后重试！");
        }
        return returnObject;
    }
    @RequestMapping("/workbench/activity/detailActivity.do")
    public String detailActivity(String id,HttpServletRequest request){
        //调用service层方法
        Activity activity = activityService.queryActivityById(id);
        List<ActivityRemark> activityRemarkList = activityRemarkService.queryActivityRemarkForDetailByActivityId(id);
        //保存数据到request中
        request.setAttribute("activity",activity);
        request.setAttribute("remarkList",activityRemarkList);
        return "workbench/activity/detail";
    }
}
