package com.javastudy.crm.workbench.service;

import com.javastudy.crm.workbench.domain.TranRemark;

import java.util.List;

public interface TranRemarkService {
    List<TranRemark> queryTranRemarkForDetailByTranId(String id);
}
