package com.javastudy.crm.workbench.web.controller;

import com.javastudy.crm.workbench.domain.Contacts;
import com.javastudy.crm.workbench.service.ContactsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.security.PublicKey;
import java.util.List;

@Controller
public class ContactsController {

    @Autowired
    private ContactsService contactsService;

    @RequestMapping("/workbench/contacts/toIndex.do")
    public String toIndex(HttpServletRequest request){
//        List<Contacts> contactsList = contactsService.queryAllContacts();
//        request.setAttribute("contacts",contactsList);
        return "/workbench/contacts/index";
    }

    @RequestMapping("/workbench/contacts/queryContacts.do")
    @ResponseBody
    public List<Contacts> queryContacts(){
        List<Contacts> contactsList = contactsService.queryAllContacts();
        return contactsList;
    }
}
