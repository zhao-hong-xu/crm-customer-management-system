package com.javastudy.crm.workbench.web.controller;

import com.fasterxml.jackson.databind.ser.std.UUIDSerializer;
import com.javastudy.crm.commons.contants.Contants;
import com.javastudy.crm.commons.domain.ReturnObject;
import com.javastudy.crm.commons.utils.DateUtils;
import com.javastudy.crm.commons.utils.UUIDUtils;
import com.javastudy.crm.settings.domain.DicValue;
import com.javastudy.crm.settings.domain.User;
import com.javastudy.crm.settings.service.DicValueService;
import com.javastudy.crm.settings.service.UserService;
import com.javastudy.crm.workbench.domain.Activity;
import com.javastudy.crm.workbench.domain.Clue;
import com.javastudy.crm.workbench.domain.ClueActivityRelation;
import com.javastudy.crm.workbench.domain.ClueRemark;
import com.javastudy.crm.workbench.service.ActivityService;
import com.javastudy.crm.workbench.service.ClueActivityRelationService;
import com.javastudy.crm.workbench.service.ClueRemarkService;
import com.javastudy.crm.workbench.service.ClueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.lang.reflect.MalformedParameterizedTypeException;
import java.util.*;

@Controller
public class ClueController {
    @Autowired
    private UserService userService;
    @Autowired
    private DicValueService dicValueService;
    @Autowired
    private ClueService clueService;
    @Autowired
    private ClueRemarkService clueRemarkService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private ClueActivityRelationService clueActivityRelationService;

    @RequestMapping("/workbench/clue/toIndex.do")
    public String toIndex(HttpServletRequest request){
        List<User> userList = userService.queryAllUsers();
        List<DicValue> appellationList = dicValueService.queryDicValueByTypeCode("appellation");
        List<DicValue> clueStateList = dicValueService.queryDicValueByTypeCode("clueState");
        List<DicValue> sourceList = dicValueService.queryDicValueByTypeCode("source");
        request.setAttribute("userList",userList);
        request.setAttribute("appellationList",appellationList);
        request.setAttribute("clueStateList",clueStateList);
        request.setAttribute("sourceList",sourceList);
        return "workbench/clue/index";
    }

    @RequestMapping("/workbench/clue/saveCreteClue.do")
    @ResponseBody
    public Object saveCreteClue(Clue clue,HttpServletRequest request){
        HttpSession session = request.getSession();
        ReturnObject returnObject=new ReturnObject();
        User user = (User) session.getAttribute(Contants.SESSION_USER);
        //封装参数
        clue.setId(UUIDUtils.getUUID());
        clue.setCreateTime(DateUtils.formateDate(new Date()));
        clue.setCreateBy(user.getId());
        //调用service层方法
        try {
            int i = clueService.saveCreateClue(clue);
            if (i>0){
                returnObject.setCode(Contants.RETURN_OBJECT_CODE_SUCCESS);
                returnObject.setRetData(clue);
            }else {
                returnObject.setCode(Contants.RETURN_OBJECT_CODE_FAIL);
                returnObject.setMassage("系统忙，请稍后重试...");
            }
        }catch (Exception e){
            e.printStackTrace();
            returnObject.setCode(Contants.RETURN_OBJECT_CODE_FAIL);
            returnObject.setMassage("系统忙，请稍后重试...");
        }
        return returnObject;
    }

    @RequestMapping("/workbench/clue/queryClueByConditionForPage.do")
    @ResponseBody
    public Object queryClueByConditionForPage(String mingcheng,String gongsi,String gongsizuoji,String xiansuolaiyuan
            ,String suoyouzhe,String shouji,String xiansuozhuangtai,int pageNo,int pageSize){
        Map<String,Object> map =new HashMap<>();
        map.put("mingcheng",mingcheng);
        map.put("gongsi",gongsi);
        map.put("gongsizuoji",gongsizuoji);
        map.put("xiansuolaiyuan",xiansuolaiyuan);
        map.put("suoyouzhe",suoyouzhe);
        map.put("shouji",shouji);
        map.put("xiansuozhuangtai",xiansuozhuangtai);
        map.put("beginNo",(pageNo-1)*pageSize);
        map.put("pageSize",pageSize);
        List<Clue> clueList = clueService.queryClueByConditionForPage(map);
        int totalRows = clueService.queryClueCountByCondition(map);
        Map<String,Object> retMap=new HashMap<>();
        retMap.put("clueList",clueList);
        retMap.put("totalRows",totalRows);
        return retMap;
    }

    @RequestMapping("/workbench/clue/deleteClueByIds.do")
    @ResponseBody
    public Object deleteClueByIds(String[] id){
        //调用service层方法
        ReturnObject returnObject=new ReturnObject();
        try {
            int i = clueService.deleteClueByIds(id);
            if (i>0){
                returnObject.setCode(Contants.RETURN_OBJECT_CODE_SUCCESS);
            }else{
                returnObject.setCode(Contants.RETURN_OBJECT_CODE_FAIL);
                returnObject.setMassage("系统忙，请稍后重试...");
            }
        }catch (Exception e){
            e.printStackTrace();
            returnObject.setCode(Contants.RETURN_OBJECT_CODE_FAIL);
            returnObject.setMassage("系统忙，请稍后重试...");
        }
        return returnObject;
    }

    @RequestMapping("/workbench/clue/queryClueById.do")
    @ResponseBody
    public Object queryClueById(String id){
        Clue clue = clueService.queryClueById(id);
        return clue;
    }

    @RequestMapping("/workbench/clue/saveEditClue.do")
    @ResponseBody
    public Object saveEditClue(Clue clue,HttpServletRequest request){
        ReturnObject returnObject=new ReturnObject();
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(Contants.SESSION_USER);
        clue.setEditBy(user.getId());
        clue.setEditTime(DateUtils.formateDate(new Date()));
        try {
            int i = clueService.saveEditClue(clue);
            if (i>0){
                returnObject.setCode(Contants.RETURN_OBJECT_CODE_SUCCESS);
            }else {
                returnObject.setCode(Contants.RETURN_OBJECT_CODE_FAIL);
                returnObject.setMassage("系统忙，请稍后重试...");
            }
        }catch (Exception e){
            e.printStackTrace();
            returnObject.setCode(Contants.RETURN_OBJECT_CODE_FAIL);
            returnObject.setMassage("系统忙，请稍后重试...");
        }
        return returnObject;
    }

    @RequestMapping("/workbench/clue/toClueDetail.do")
    public String toClueDetail(String id,HttpServletRequest request){
        //调用service层方法
        //查询线索明细
        Clue clue = clueService.queryClueForDetailById(id);
        //查询线索备注列表
        List<ClueRemark> clueRemarkList = clueRemarkService.queryClueRemarkForDetailByClueId(id);
        //查询线索关联的市场活动
        List<Activity> activityList = activityService.queryActivityForDetailByClueId(id);
        //封装数据转发
        request.setAttribute("clue",clue);
        request.setAttribute("remarkList",clueRemarkList);
        request.setAttribute("activityList",activityList);

        return "workbench/clue/detail";
    }

    @RequestMapping("/workbench/clue/queryActivityByNameAndClueId.do")
    @ResponseBody
    public Object queryActivityByNameAndClueId(String activityName,String clueId){
        //封装参数
        Map<String,Object> map =new HashMap<>();
        map.put("activityName",activityName);
        map.put("clueId",clueId);
        //调用service层方法，查询市场活动
        List<Activity> activityList = activityService.queryActivityForDetailByNameAndClueId(map);
        return activityList;
    }

    @RequestMapping("/workbench/clue/relateClueAndActivity.do")
    @ResponseBody
    public Object relateClueAndActivity(String[] activityId,String clueId){
        List<ClueActivityRelation> list=new ArrayList<>();
        ReturnObject returnObject=new ReturnObject();
        ClueActivityRelation car =null;
      for (String id:activityId){
          car =new ClueActivityRelation();
          car.setId(UUIDUtils.getUUID());
          car.setActivityId(id);
          car.setClueId(clueId);
          list.add(car);
      }
      try {
          int i = clueActivityRelationService.saveRelateClueAndActivity(list);

          if (i>0){
              returnObject.setCode(Contants.RETURN_OBJECT_CODE_SUCCESS);
              List<Activity> activityList = activityService.queryActivitiesForDetailByIds(activityId);
              returnObject.setRetData(activityList);
          }else {
              returnObject.setCode(Contants.RETURN_OBJECT_CODE_FAIL);
              returnObject.setMassage("系统忙，请稍后重试...");
          }
      }catch (Exception e){
          e.printStackTrace();
          returnObject.setCode(Contants.RETURN_OBJECT_CODE_FAIL);
          returnObject.setMassage("系统忙，请稍后重试...");
      }
      return returnObject;
    }

    @RequestMapping("/workbench/clue/unbindClueAndActivity.do")
    @ResponseBody
    public Object unbindClueAndActivity(ClueActivityRelation clueActivityRelation){
        ReturnObject returnObject=new ReturnObject();
        try {
            int i = clueActivityRelationService.deleteClueActivityRelationByClueIdAndActivityId(clueActivityRelation);
            if (i>0){
                returnObject.setCode(Contants.RETURN_OBJECT_CODE_SUCCESS);
            }else {
                returnObject.setCode(Contants.RETURN_OBJECT_CODE_FAIL);
                returnObject.setMassage("系统忙，请稍后重试...");
            }
        }catch (Exception e){
            e.printStackTrace();
            returnObject.setCode(Contants.RETURN_OBJECT_CODE_FAIL);
            returnObject.setMassage("系统忙，请稍后重试...");
        }
        return returnObject;
    }

    @RequestMapping("/workbench/clue/toConvert.do")
    public String toConvert(String id,HttpServletRequest request){
        Clue clue = clueService.queryClueForDetailById(id);
        List<DicValue> dicValueList = dicValueService.queryDicValueByTypeCode("stage");
        request.setAttribute("clue",clue);
        request.setAttribute("stageList",dicValueList);
        return "workbench/clue/convert";
    }

    @RequestMapping("/workbench/clue/queryActivityByActivityNameAndClueId.do")
    @ResponseBody
    public Object queryActivityByActivityNameAndClueId(String activityName,String clueId){
        //封装参数
        Map<String,Object> map =new HashMap<>();
        map.put("activityName",activityName);
        map.put("clueId",clueId);
        //调用service层方法，查询符合条件的市场活动
        List<Activity> activityList = activityService.queryActivityForConvertByActivityNameAndClueId(map);
        return activityList;
    }
    @RequestMapping("/workbench/clue/convertClue.do")
    @ResponseBody
    public Object convertClue(String clueId,String money,String name,String expectedDate,String stage,
                              String activityId,String isCreateTran,HttpServletRequest request){
      ReturnObject returnObject=new ReturnObject();
        HttpSession session = request.getSession();

        //封装参数
      Map<String,Object> map=new HashMap<>();
      map.put("clueId",clueId);
      map.put("money",money);
      map.put("name",name);
      map.put("expectedDate",expectedDate);
      map.put("stage",stage);
      map.put("activityId",activityId);
      map.put("isCreateTran",isCreateTran);
      map.put(Contants.SESSION_USER,session.getAttribute(Contants.SESSION_USER));
      //调用service层方法
        try {
            clueService.saveConvert(map);
            returnObject.setCode(Contants.RETURN_OBJECT_CODE_SUCCESS);
        }catch (Exception e){
            e.printStackTrace();
            returnObject.setCode(Contants.RETURN_OBJECT_CODE_FAIL);
            returnObject.setMassage("系统忙，请稍后重试...");
        }
    return returnObject;
    }


}
