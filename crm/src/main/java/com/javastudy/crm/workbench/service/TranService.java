package com.javastudy.crm.workbench.service;

import com.javastudy.crm.workbench.domain.FunnelVO;
import com.javastudy.crm.workbench.domain.Tran;

import java.util.List;
import java.util.Map;

public interface TranService {
    void saveCreateTransaction(Map<String,Object> map);
    Tran selectTranForDetailById(String id);
    List<FunnelVO> queryCountOfTranGroupByStage();
    List<Tran> selectTrans();
}
