package com.javastudy.crm.workbench.service.impl;

import com.javastudy.crm.commons.contants.Contants;
import com.javastudy.crm.commons.utils.DateUtils;
import com.javastudy.crm.commons.utils.UUIDUtils;
import com.javastudy.crm.settings.domain.User;
import com.javastudy.crm.workbench.domain.*;
import com.javastudy.crm.workbench.mapper.CustomerMapper;
import com.javastudy.crm.workbench.mapper.TranHistoryMapper;
import com.javastudy.crm.workbench.mapper.TranMapper;
import com.javastudy.crm.workbench.service.CustomerService;
import com.javastudy.crm.workbench.service.TranService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;
@Service
public class TranServiceImpl implements TranService {
    @Autowired
    private CustomerMapper customerMapper;
    @Autowired
    private TranMapper tranMapper;
    @Autowired
    private TranHistoryMapper tranHistoryMapper;
    @Override
    public void saveCreateTransaction(Map<String, Object> map) {
        String customerName= (String) map.get("customerName");
        User user= (User) map.get(Contants.SESSION_USER);
        //根据客户名字查询客户信息
        Customer customer=customerMapper.selectCustomerByName(customerName);
        //如果客户不存在，则新建一个客户
        if (customer==null){
            customer=new Customer();
            customer.setId(UUIDUtils.getUUID());
            customer.setOwner(user.getId());
            customer.setName(customerName);
            customer.setCreateBy(user.getId());
            customer.setCreateTime(DateUtils.formateDate(new Date()));
            customerMapper.insertCreateCustomer(customer);
        }
        //保存创建的交易
        Tran tran=new Tran();
        tran.setId(UUIDUtils.getUUID());
        tran.setOwner((String) map.get("owner"));
        tran.setMoney((String) map.get("money"));
        tran.setName((String) map.get("name"));
        tran.setExpectedDate((String) map.get("expectedDate"));
        tran.setCustomerId(customer.getId());
        tran.setStage((String) map.get("stage"));
        tran.setType((String) map.get("type"));
        tran.setSource((String) map.get("source"));
        tran.setActivityId((String) map.get("activityId"));
        tran.setContactsId((String) map.get("contactsId"));
        tran.setCreateBy(user.getId());
        tran.setCreateTime(DateUtils.formateDate(new Date()));
        tran.setDescription((String) map.get("description"));
        tran.setContactSummary((String) map.get("contactSummary"));
        tran.setNextContactTime((String) map.get("nextContactTime"));
        int i = tranMapper.insertCreateTran(tran);
        //保存交易记录到交易历史记录里面一份
        TranHistory tranHistory=new TranHistory();
        tranHistory.setId(UUIDUtils.getUUID());
        tranHistory.setStage(tran.getStage());
        tranHistory.setMoney(tran.getMoney());
        tranHistory.setExpectedDate(tran.getExpectedDate());
        tranHistory.setCreateTime(DateUtils.formateDate(new Date()));
        tranHistory.setCreateBy(user.getId());
        tranHistory.setTranId(tran.getId());
        tranHistoryMapper.insertTranHistory(tranHistory);
    }

    @Override
    public Tran selectTranForDetailById(String id) {
        return tranMapper.selectTranForDetailById(id);
    }

    @Override
    public List<FunnelVO> queryCountOfTranGroupByStage() {
        return tranMapper.selectCountOfTranGroupByStage();
    }

    @Override
    public List<Tran> selectTrans() {
        return tranMapper.selectTrans();
    }
}
