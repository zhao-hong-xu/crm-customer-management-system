package com.javastudy.crm.workbench.mapper;

import com.javastudy.crm.workbench.domain.Activity;
import org.springframework.jdbc.support.nativejdbc.OracleJdbc4NativeJdbcExtractor;

import java.util.List;
import java.util.Map;

public interface ActivityMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_activity
     *
     * @mbggenerated Wed Jun 08 16:18:51 CST 2022
     */
    int deleteByPrimaryKey(String id);



    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_activity
     *
     * @mbggenerated Wed Jun 08 16:18:51 CST 2022
     */
    int insertSelective(Activity record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_activity
     *
     * @mbggenerated Wed Jun 08 16:18:51 CST 2022
     */
    Activity selectByPrimaryKey(String id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_activity
     *
     * @mbggenerated Wed Jun 08 16:18:51 CST 2022
     */
    int updateByPrimaryKeySelective(Activity record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_activity
     *
     * @mbggenerated Wed Jun 08 16:18:51 CST 2022
     */
    int updateByPrimaryKey(Activity record);
    /**
     * 插入用户新建的活动到数据库
     */
    int insertActivity(Activity activity);

    /**
     * 根据页数条件查询活动集合
     * @param map
     * @return
     */
    List<Activity> selectActivityByConditionForPage(Map<String,Object> map);

    /**
     *根据条件查询符合条件的条数
     */
    Integer selectCountOfActivityByCondition(Map<String, Object> map);

    /**
     * 根据id删除市场活动
     * @param ids
     * @return
     */
    int deleteActivityByIds(String[] ids);

    /**
     * 根据id查询市场活动
     * @param id
     * @return
     */
    Activity selectActivityById(String id);

    /**
     * 修改市场活动信息
     * @param activity
     * @return
     */
    int updateActivity(Activity activity);

    /**
     * 查询所有市场活动信息
     * @return
     */
    List<Activity> selectAllActivities();

    /**
     * 根据多个id查询市场活动信息列表
     * @param ids
     * @return
     */
    List<Activity> selectXzActivitiesByIds(String[] ids);

    /**
     * 保存导入操作上传过来的市场活动信息
     * @param activityList
     * @return
     */
    int insertActivityByList(List<Activity> activityList);

    /**
     * 根据id查询所有信息
     * @param id
     * @return
     */
    Activity selectActivityForDetailById(String id);

    /**
     * 根据线索id查询与其关联的市场活动的信息
     * @param clueId
     * @return
     */
    List<Activity> selectActivityForDetailByClueId(String clueId);

    /**
     * 根据关键字模糊查询包含此关键字但不包含已经用线索id查询过的市场活动信息
     * @param map
     * @return
     */
    List<Activity> selectActivityForDetailByNameAndClueId(Map<String,Object> map);

    /**
     * 根据多个id查询市场活动列表
     * @param ids
     * @return
     */
    List<Activity> selectActivitiesForDetailByIds(String[] ids);

    /**
     * 根据关键字模糊查询包含此关键字并且包含已经用线索id查询过的市场活动信息
     * @param map
     * @return
     */
    List<Activity> selectActivityForConvertByActivityNameAndClueId(Map<String,Object> map);
}
