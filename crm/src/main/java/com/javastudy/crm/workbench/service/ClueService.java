package com.javastudy.crm.workbench.service;

import com.javastudy.crm.workbench.domain.Clue;

import java.util.List;
import java.util.Map;

public interface ClueService {
    int saveCreateClue(Clue clue);
    List<Clue> queryClueByConditionForPage(Map<String,Object> map);
    int queryClueCountByCondition(Map<String,Object> map);
    int deleteClueByIds(String[] ids);
    Clue queryClueById(String id);
    int saveEditClue(Clue clue);
    Clue queryClueForDetailById(String id);
    void saveConvert(Map<String,Object> map);
}
