package com.javastudy.crm.workbench.service;

import com.javastudy.crm.workbench.domain.Contacts;

import java.util.List;

public interface ContactsService {
    List<Contacts> queryAllContacts();
}
