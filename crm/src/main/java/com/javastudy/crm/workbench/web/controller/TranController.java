package com.javastudy.crm.workbench.web.controller;

import com.javastudy.crm.commons.contants.Contants;
import com.javastudy.crm.commons.domain.ReturnObject;
import com.javastudy.crm.settings.domain.DicValue;
import com.javastudy.crm.settings.domain.User;
import com.javastudy.crm.settings.service.DicValueService;
import com.javastudy.crm.settings.service.UserService;


import com.javastudy.crm.workbench.domain.Tran;
import com.javastudy.crm.workbench.domain.TranHistory;
import com.javastudy.crm.workbench.domain.TranRemark;
import com.javastudy.crm.workbench.service.CustomerService;
import com.javastudy.crm.workbench.service.TranHistoryService;
import com.javastudy.crm.workbench.service.TranRemarkService;
import com.javastudy.crm.workbench.service.TranService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;


@Controller
public class TranController {
     @Autowired
     private DicValueService dicValueService;
     @Autowired
     private UserService userService;
     @Autowired
     private CustomerService customerService;
     @Autowired
     private TranService tranService;
     @Autowired
     private TranRemarkService tranRemarkService;
     @Autowired
     private TranHistoryService tranHistoryService;


    @RequestMapping("/workbench/transaction/toIndex.do")
    public String toIndex(HttpServletRequest request){
        //调用service层方法，查询数据字典值
        List<DicValue> transactionTypeList = dicValueService.queryDicValueByTypeCode("transactionType");
        List<DicValue> sourceList = dicValueService.queryDicValueByTypeCode("source");
        List<DicValue> stageList = dicValueService.queryDicValueByTypeCode("stage");
        //封装数据到request请求域中
        request.setAttribute("transactionTypeList",transactionTypeList);
        request.setAttribute("sourceList",sourceList);
        request.setAttribute("stageList",stageList);
        //请求转发
        return "workbench/transaction/index";
    }

    @RequestMapping("/workbench/transaction/toSave.do")
    public String toSave(HttpServletRequest request){
        //调用service层方法，查询数据字典值
        List<DicValue> transactionTypeList = dicValueService.queryDicValueByTypeCode("transactionType");
        List<DicValue> sourceList = dicValueService.queryDicValueByTypeCode("source");
        List<DicValue> stageList = dicValueService.queryDicValueByTypeCode("stage");
        List<User> userList = userService.queryAllUsers();
        //封装数据到request请求域中
        request.setAttribute("transactionTypeList",transactionTypeList);
        request.setAttribute("sourceList",sourceList);
        request.setAttribute("stageList",stageList);
        request.setAttribute("userList",userList);
        //请求转发
        return "workbench/transaction/save";
    }

    @RequestMapping("/workbench/transaction/queryPossibility.do")
    @ResponseBody
    public Object queryPossibility(String stageValue){
        //根据阶段名称查询该阶段对应的值ResourceBundle bundle=ResourceBundle.getBundle("possibility");
        //        String possibility=bundle.getString(stageValue);
        // 属性资源文件绑定
        String possName="possibility";
         ResourceBundle bundle = ResourceBundle.getBundle(possName);
         String possibilityValue = bundle.getString(stageValue);
         return possibilityValue;
    }

    @RequestMapping("/workbench/transaction/queryAllCustomer.do")
    @ResponseBody
    public Object queryAllCustomerForSave(String customerName){
        //调用service层方法，查询所有客户信息
        List<String> customerNameList = customerService.queryAllCustomerNameByName(customerName);
        return customerNameList;
    }
    //@RequestParam注解是用来将参数自动封装到map集合中，参数中的key作map的key；参数中的value作map的value
    @RequestMapping("/workbench/transation/saveCreateTran.do")
    @ResponseBody
    public Object saveCreateTran(@RequestParam Map<String,Object> map,HttpServletRequest request){

        ReturnObject returnObject=new ReturnObject();
        //将用户的信息也封装到map集合中
        HttpSession session = request.getSession();
        map.put(Contants.SESSION_USER,session.getAttribute(Contants.SESSION_USER));
        //调用service层方法，保存创建的交易记录
        //利用是否报异常来判断service层是否顺利执行
        try{
            tranService.saveCreateTransaction(map);
            returnObject.setCode(Contants.RETURN_OBJECT_CODE_SUCCESS);
        }catch (Exception e){
            e.printStackTrace();
            returnObject.setCode(Contants.RETURN_OBJECT_CODE_FAIL);
            returnObject.setMassage("系统忙，请稍后再试...");
        }
        return returnObject;

    }

    @RequestMapping("workbench/tran/toDetail.do")
    public String toDetail(String id,HttpServletRequest request){
        //调用service层方法，查询数据
        Tran tran = tranService.selectTranForDetailById(id);
        List<TranRemark> tranRemarkList = tranRemarkService.queryTranRemarkForDetailByTranId(id);
        List<TranHistory> tranHistories = tranHistoryService.queryTranHistoryForDetailByTranId(id);
        //根据tran所处阶段查询可能性
        String possName="possibility";
        /*ResourceBundle bundle = ResourceBundle.getBundle(possName);
        String possibility = bundle.getString(tran.getStage());*/
        /*tran.setPossibility(possibility);*/
        //根据数据字典type，查询字典值
        List<DicValue> stageList = dicValueService.queryDicValueByTypeCode("stage");
        //把数据保存到request请求域中
        request.setAttribute("tran",tran);
        request.setAttribute("remarkList",tranRemarkList);
        request.setAttribute("historyList",tranHistories);
        request.setAttribute("stageList",stageList);
        //请求转发
        return "workbench/transaction/detail";
    }

    @RequestMapping("/workbench/transaction/queryTransation.do")
    @ResponseBody
    public List<Tran> queryTrans(){
        List<Tran> trans = tranService.selectTrans();
        return trans;
    }
}
