package com.javastudy.crm.workbench.service.impl;

import com.javastudy.crm.workbench.domain.Customer;
import com.javastudy.crm.workbench.mapper.CustomerMapper;
import com.javastudy.crm.workbench.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    private CustomerMapper customerMapper;
    @Override
    public List<Customer> queryAllCustomer() {
        return customerMapper.selectAllCustomer() ;
    }

    @Override
    public List<String> queryAllCustomerNameByName(String name) {
        return customerMapper.selectAllCustomerName(name);
    }
}
