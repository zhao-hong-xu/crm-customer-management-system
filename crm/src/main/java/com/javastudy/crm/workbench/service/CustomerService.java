package com.javastudy.crm.workbench.service;

import com.javastudy.crm.workbench.domain.Customer;

import java.util.List;

public interface CustomerService {
    List<Customer> queryAllCustomer();
    List<String> queryAllCustomerNameByName(String name);

}
