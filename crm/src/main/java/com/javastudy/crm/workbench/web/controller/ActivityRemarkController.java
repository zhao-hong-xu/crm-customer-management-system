package com.javastudy.crm.workbench.web.controller;

import com.javastudy.crm.commons.contants.Contants;
import com.javastudy.crm.commons.domain.ReturnObject;
import com.javastudy.crm.commons.utils.DateUtils;
import com.javastudy.crm.commons.utils.UUIDUtils;
import com.javastudy.crm.settings.domain.User;
import com.javastudy.crm.workbench.domain.ActivityRemark;
import com.javastudy.crm.workbench.service.ActivityRemarkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;

@Controller
public class ActivityRemarkController {
    @Autowired
    private ActivityRemarkService activityRemarkService;

    @RequestMapping("/workbench/activity/saveCreateActivityRemark.do")
    @ResponseBody
    public Object saveCreateActivityRemark(ActivityRemark activityRemark, HttpServletRequest request){
        HttpSession session = request.getSession();
        User user= (User) session.getAttribute(Contants.SESSION_USER);
        //封装参数
        activityRemark.setId(UUIDUtils.getUUID());
        activityRemark.setCreateBy(user.getId());
        activityRemark.setCreateTime(DateUtils.formateDate(new Date()));
        activityRemark.setEditFlag(Contants.REMARK_EDIT_FLAG_NO_EDITED);
        ReturnObject returnObject=new ReturnObject();
        //调用service层方法，保存创建的市场活动备注
        try {
            int i = activityRemarkService.saveCreateActivityRemark(activityRemark);
            if (i>0){
                returnObject.setCode(Contants.RETURN_OBJECT_CODE_SUCCESS);
                returnObject.setRetData(activityRemark);
            }else{
                returnObject.setCode(Contants.RETURN_OBJECT_CODE_FAIL);
                returnObject.setMassage("系统忙，请稍后重试！");
            }
        }catch (Exception e){
            e.printStackTrace();
            returnObject.setCode(Contants.RETURN_OBJECT_CODE_FAIL);
            returnObject.setMassage("系统忙，请稍后重试！");
        }
        return returnObject;
    }

    @RequestMapping("/workbench/activity/deleteActivityById.do")
    @ResponseBody
    public Object deleteActivityById(String id){
        ReturnObject returnObject=new ReturnObject();
        try{
            int i=activityRemarkService.deleteActivityRemarkById(id);
            if (i>0){
                returnObject.setCode(Contants.RETURN_OBJECT_CODE_SUCCESS);
             }else{
                returnObject.setCode(Contants.RETURN_OBJECT_CODE_FAIL);
                returnObject.setMassage("系统忙，请稍后重试...");
             }
            }catch (Exception e){
            e.printStackTrace();
            returnObject.setCode(Contants.RETURN_OBJECT_CODE_FAIL);
            returnObject.setMassage("系统忙，请稍后重试...");
        }
        return returnObject;
    }

    @RequestMapping("/workbench/activity/updateActivityRemark.do")
    @ResponseBody
    public Object updateActivityRemark(ActivityRemark activityRemark,HttpServletRequest request){
      ReturnObject returnObject=new ReturnObject();
      HttpSession session = request.getSession();
      User user= (User) session.getAttribute(Contants.SESSION_USER);
      activityRemark.setEditFlag(Contants.REMARK_EDIT_FLAG_YES_EDITED);
      activityRemark.setEditBy(user.getId());
      activityRemark.setEditTime(DateUtils.formateDate(new Date()));
      try {
          int i = activityRemarkService.updateActivityRemark(activityRemark);
          if (i>0){
              returnObject.setCode(Contants.RETURN_OBJECT_CODE_SUCCESS);
              returnObject.setRetData(activityRemark);
          }else {
              returnObject.setCode(Contants.RETURN_OBJECT_CODE_FAIL);
              returnObject.setMassage("系统忙，请稍后重试...");

          }
      }catch (Exception e){
          e.printStackTrace();
          returnObject.setCode(Contants.RETURN_OBJECT_CODE_FAIL);
          returnObject.setMassage("系统忙，请稍后重试...");
      }
      return returnObject;
    }
}
