package com.javastudy.crm.workbench.service;

import com.javastudy.crm.workbench.domain.Activity;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import java.util.List;
import java.util.Map;

public interface ActivityService {
    //保存新创建的市场活动
    int saveCreateActivity(Activity activity);
    //根据分业条件查询市场活动集合
    List<Activity> queryActivityByConditionForPage(Map<String,Object> map);
    //根据条件查询活动个数
    Integer queryCountOfActivityByCondition(Map<String,Object> map);
    //根据id删除市场活动
    int deleterActivityByIds(String[] ids);
    //根据id查市场活动
    Activity queryActivityById(String id);
    //保存修改的市场活动信息
    int saveEditActivity(Activity activity);
    //查询所有市场活动信息
    List<Activity> queryAllActivities();
    //根据id查询被选中的市场活动信息
    List<Activity> queryXzActivitiesByIds(String[] ids);
    //保存导入的市场活动数据
    int saveActivitiesByList(List<Activity> activityList);
    //根据id查询市场活动信息明细
    Activity queryActivityForDetailById(String id);
    //根据线索id查询与其相关的市场活动信息
    List<Activity> queryActivityForDetailByClueId(String clueId);
    //根据关键字模糊查询包含此关键字但不包含已经用线索id查询过的市场活动信息
    List<Activity> queryActivityForDetailByNameAndClueId(Map<String,Object> map);
    //根据多个id查询市场活动
    List<Activity> queryActivitiesForDetailByIds(String[] ids);
    //根据关键字模糊查询包含此关键字并且包含已经用线索id查询过的市场活动信息
    List<Activity> queryActivityForConvertByActivityNameAndClueId(Map<String,Object> map);

}
