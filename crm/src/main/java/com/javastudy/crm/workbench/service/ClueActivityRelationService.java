package com.javastudy.crm.workbench.service;

import com.javastudy.crm.workbench.domain.ClueActivityRelation;

import java.util.List;

public interface ClueActivityRelationService {
    //保存关联的市场活动和线索
    int saveRelateClueAndActivity(List<ClueActivityRelation> list);
    //根据市场活动id和线索id删除关联关系
    int deleteClueActivityRelationByClueIdAndActivityId(ClueActivityRelation clueActivityRelation);
}
