package com.javastudy.crm.settings.web.controller;

import com.javastudy.crm.commons.contants.Contants;
import com.javastudy.crm.commons.domain.ReturnObject;
import com.javastudy.crm.commons.utils.DateUtils;
import com.javastudy.crm.settings.domain.User;
import com.javastudy.crm.settings.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Controller
public class UserController {
    //自动注入业务逻辑层对象
    @Autowired
    private UserService userService;

    //进入到登录界面
    @RequestMapping("/settings/qx/user/toLogin.do")
    public  String toLogin(){
        return  "settings/qx/user/login";
    }

    //处理登录界面发送过来的数据并处理
    @RequestMapping("/settings/qx/user/login.do")
    @ResponseBody
    public  Object login(String loginAct, String loginPwd, String isRemPwd,
                         HttpServletResponse response, HttpServletRequest request){
        //封装接收的参数
        Map<String,Object> map=new HashMap<>();
        map.put("loginAct",loginAct);
        map.put("loginPwd",loginPwd);
        //调用service层方法，查询用户
        User user = userService.queryUserByLoginActAndPwd(map);
        ReturnObject returnObject = new ReturnObject();
        String loginSuccess = Contants.RETURN_OBJECT_CODE_SUCCESS;
        String loginFail = Contants.RETURN_OBJECT_CODE_FAIL;

        //根据查询结果，响应不同信息
        if (user==null){
            //用户名或密码错误
            returnObject.setCode("0");
            returnObject.setMassage("用户名或密码错误!");
        }else{ //判断该用户是否合法：是否超过有效期；是否是被锁定状态；IP地址是否合法

            String currentTime = DateUtils.formateDate(new Date());
            if (currentTime.compareTo(user.getExpireTime())>0){
                //该用户已过期
                returnObject.setCode(loginFail);
                returnObject.setMassage("账号已过期!");
            }else if ("0".equals(user.getLockState())){
                //该用户处于死锁状态
                returnObject.setCode(loginFail);
                returnObject.setMassage("账号状态已被锁定!");
            }else if (! user.getAllowIps().contains(request.getRemoteAddr())){
                //本次访问的IP地址不在数据库当中
                returnObject.setCode(loginFail);
                returnObject.setMassage("IP受限!");
            }else {
                //表示成功登录
                returnObject.setCode(loginSuccess);
                HttpSession session = request.getSession();
                session.setAttribute(Contants.SESSION_USER, user);
                if ("true".equals(isRemPwd)) {
                    Cookie c1 = new Cookie("loginAct", user.getLoginAct());
                    c1.setMaxAge(60 * 60 * 24 * 10);
  //                  c1.setPath(request.getContextPath());
                    response.addCookie(c1);
                    Cookie c2 = new Cookie("loginPwd", user.getLoginPwd());
                    c2.setMaxAge(60 * 60 * 24 * 10);
 //                   c2.setPath(request.getContextPath());
                    response.addCookie(c2);
                }else{
                    Cookie c1 = new Cookie("loginAct", "1");
                    c1.setMaxAge(0);
 //                   c1.setPath(request.getContextPath());
                    response.addCookie(c1);
                    Cookie c2 = new Cookie("loginPwd","1");
                    c2.setMaxAge(0);
//                    c2.setPath(request.getContextPath());
                    response.addCookie(c2);
                }
            }
        }
        return returnObject;
    }

    /**
     * 用户安全退出
     */
    @RequestMapping("/settings/qx/user/logout.do")
    public String logout(HttpServletResponse response,HttpServletRequest request){
        //清空cookie
        Cookie c1 = new Cookie("loginAct", "1");
        c1.setMaxAge(0);
     //   c1.setPath(request.getContextPath());
        response.addCookie(c1);
        Cookie c2 = new Cookie("loginPwd","1");
        c2.setMaxAge(0);
     //   c2.setPath(request.getContextPath());
        response.addCookie(c2);
        //销毁session
        request.getSession().invalidate();
        return "redirect:/";
    }
}
