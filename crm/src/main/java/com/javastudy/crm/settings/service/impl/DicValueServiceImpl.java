package com.javastudy.crm.settings.service.impl;

import com.javastudy.crm.settings.domain.DicValue;
import com.javastudy.crm.settings.mapper.DicValueMapper;
import com.javastudy.crm.settings.service.DicValueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DicValueServiceImpl implements DicValueService {
    @Autowired
    private DicValueMapper dicValueMapper;
    @Override
    public List<DicValue> queryDicValueByTypeCode(String typeCode) {
        return dicValueMapper.selectDicValueByTypeCode(typeCode);
    }
}
