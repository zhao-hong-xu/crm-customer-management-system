package com.javastudy.crm.settings.service;

import com.javastudy.crm.settings.domain.User;

import java.util.List;
import java.util.Map;

public interface UserService {
    //根据map参数loginAct和loginPwd查询用户
    User queryUserByLoginActAndPwd(Map<String,Object> map);
    //查询所有用户
    List<User> queryAllUsers();
}
