package com.javastudy.crm.commons.utils;

import java.io.*;
import java.math.BigDecimal;
import java.util.*;

public class Test {
   /* public static void main(String[] args) {
        int[] data = {3,1,6,2,5};
        Arrays.sort(data);
        for (int i=0; i<data.length; i++) {
            System.out.println(data[i]); }
        System.out.println("");
        System.out.println(data);
        int index = Arrays.binarySearch(data, 3);
        System.out.println("index=" + index);
    }*/
  /* public static void main(String[] args) throws Exception{
       BigDecimal v1 = new BigDecimal(10);
       BigDecimal v2 = new BigDecimal(20);
//相加运算
       BigDecimal v3 = v1.add(v2);
       System.out.println(v3);
   }*/
 /*  public static void main(String[] args) {
       int i1 = 100;
       int i2 = 10;
       try {
           int i3 = i1/i2;
           System.out.println(i3);
           return;
       }catch(ArithmeticException ae) {
           ae.printStackTrace();
   }
   finally {
//会执行 finally
        System.out.println("----------finally---------");
    }
}*/
  /* public static void main(String[] args) {
       int i = method1();
       System.out.println(i);
   }

    private static int method1() {
        int a = 10;
        try {
             a=50;
        }finally {
            a = 100;
        }
        return a;
   }*/
/*   public static void main(String[] args) {
       Map map = new HashMap();
       map.put("1001", "张三");
       map.put("1002", "李四");
       map.put("1003", "王五");
//采用 entrySet 遍历 Map
       Set entrySet = map.entrySet();
       for (Iterator iter = entrySet.iterator(); iter.hasNext();) {
           Map.Entry entry = (Map.Entry)iter.next();

           System.out.println(entry.getKey() + ", " + entry.getValue());
       }
       System.out.println("");
//取得 map 中指定的 key
       Object v = map.get("1003");
       System.out.println("1003==" + v);
       System.out.println("");
//如果存在相同的条目，会采用此条目替换
//但 map 中始终保持的是不重复的数据
//主要依靠 key 开判断是否重复，和 value 没有任何关系
       map.put("1003", "赵柳");
//采用 keySet 和 get 取得 map 中的所有数据
       for (Iterator iter=map.keySet().iterator(); iter.hasNext();) {
           String k = (String)iter.next();
           System.out.println(k + ", " + map.get(k));
       } }*/
  /* public static void main(String[] args) {
       Reader is = null;
       try {
           is = new FileReader("C:\\Users\\86187\\Desktop\\Java学习笔记\\1.txt");
           int b = 0;
           while ((b = is.read()) != -1) {
//直接打印

//System.out.print(b);
//输出字符
               System.out.print((char)b);
           }
       }catch(FileNotFoundException e) {
           e.printStackTrace();
       }catch(IOException e) {
           e.printStackTrace();
       }finally {
           try {
               if (is != null){
                   is.close();
               }
           }catch(IOException e) {}
       }
   }*/
   public static String test1(){
    try{
        System.out.println("try");
        return "return";
    }
        catch(Exception e){
        e.printStackTrace();

    }finally{
        System.out.println("finally");
    }
        return null;
   }


    public static void main(String[] args) {
        String s = test1();
        System.out.println(s
        );
    }

    }
