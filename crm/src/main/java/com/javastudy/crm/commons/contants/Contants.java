package com.javastudy.crm.commons.contants;


/**
 * 定义返回值code的常量
 */
public class Contants {
    public static final String RETURN_OBJECT_CODE_SUCCESS="1"; //登录成功
    public static final String RETURN_OBJECT_CODE_FAIL="0"; //登录失败

    //保存user的session域名
    public static final String SESSION_USER="sessionUser";

    //备注的修改标记
    public static final  String REMARK_EDIT_FLAG_NO_EDITED="0"; //没被修改过
    public static final  String REMARK_EDIT_FLAG_YES_EDITED="1"; //已经被修改过

}
