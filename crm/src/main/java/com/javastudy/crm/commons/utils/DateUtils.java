package com.javastudy.crm.commons.utils;


import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * date日期相关处理工具类
 */
public class DateUtils {
    public static String formateDate(Date dateTime){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String timeStr = sdf.format(dateTime);
        return timeStr;
    }
}
